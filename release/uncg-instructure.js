/*
* uncg-instructure.js
* version 1.9 (10/30/2020)
*/

/**
* Function that tracks a click on an outbound link in Analytics.
* This function takes a valid URL string as an argument, and uses that URL string
* as the event label. Setting the transport method to 'beacon' lets the hit be sent
* using 'navigator.sendBeacon' in browser that support it.
*
* Augmented from example at https://support.google.com/analytics/answer/1136920?hl=en
* to include passing in an Event Action for easier tracking of where the link was clicked
* on.
*
*/

// Source: https://community.canvaslms.com/t5/Admin-Group/How-to-Set-Up-Google-Analytics-for-Canvas/ba-p/245230
// Updated Aug 28, 2019
// In Google Analytics you'll need to set up custom dimensions as follows
// Custom Dimension 1 = Canvas User ID --- Scope = User
// Custom Dimension 2 = Archived --- Scope = User
// Custom Dimension 3 = Canvas User Role --- Scope = User
// Custom Dimension 4 = Canvas Course ID --- Scope = Hit
// Custom Dimension 5 = Canvas Course Name --- Scope = Hit
// Custom Dimension 6 = Canvas Sub-Account ID --- Scope = Hit
// Custom Dimension 7 = Canvas Term ID --- = Scope = Hit
// Custom Dimension 8 = Canvas Course Role --- Scope = Hit

(function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'custom_ga');

// custom_ga('create', 'UA-34361385-4', 'auto');
// custom_ga('send', 'pageview');

function removeStorage(key) {
    try {
        localStorage.removeItem(key);
        localStorage.removeItem(key + '_expiresIn');
    } catch (e) {
        console.log('removeStorage: Error removing key [' + key + '] from localStorage: ' + JSON.stringify(e));
        return false;
    }
    return true;
}

function getStorage(key) {
    var now = Date.now(); //epoch time, lets deal only with integer
    // set expiration for storage
    var expiresIn = localStorage.getItem(key + '_expiresIn');
    if (expiresIn === undefined || expiresIn === null) {
        expiresIn = 0;
    }

    if (expiresIn < now) { // Expired
        removeStorage(key);
        return null;
    } else {
        try {
            var value = localStorage.getItem(key);
            return value;
        } catch (e) {
            console.log('getStorage: Error reading key [' + key + '] from localStorage: ' + JSON.stringify(e));
            return null;
        }
    }
}

function setStorage(key, value, expires) {
    if (expires === undefined || expires === null) {
        expires = (24 * 60 * 60); // default: seconds for 6 hours (6*60*60)
    } else {
        expires = Math.abs(expires); //make sure it's positive
    }

    var now = Date.now(); //millisecs since epoch time, lets deal only with integer
    var schedule = now + expires * 1000;
    try {
        localStorage.setItem(key, value);
        localStorage.setItem(key + '_expiresIn', schedule);
    } catch (e) {
        console.log('setStorage: Error setting key [' + key + '] in localStorage: ' + JSON.stringify(e));
        return false;
    }
    return true;
}

async function coursesRequest(courseId) {
    //
    let response = await fetch('/api/v1/users/self/courses?per_page=100');
    let data = await response.text();
    data = data.substr(9);
    data = JSON.parse(data)
    var stringData = JSON.stringify(data)
    setStorage('ga_enrollments', stringData, null)
    var course = parseCourses(courseId, stringData)
    return course
};

function parseCourses(courseId, courseData) {
    if (courseData != undefined) {
        let data = JSON.parse(courseData);
        //console.log(data)
        for (var i = 0; i < data.length; i++) {
            // console.log(data[i]['id'] + " " + courseId)
            if (data[i]['id'] == courseId) {
                return data[i]
            }
        }
    }
    return null
}

function gaCourseDimensions(course) {
    // console.log('course data being set');
    // console.log(course);
    custom_ga('set', 'dimension3', course['id']);
    // console.log(course['id']);
    custom_ga('set', 'dimension4', course['name']);
    // console.log(course['name']);
    custom_ga('set', 'dimension5', course['account_id']);
    // console.log(course['account_id']);
    custom_ga('set', 'dimension6', course['enrollment_term_id']);
    // console.log(course['enrollment_term_id']);
    custom_ga('set', 'dimension7', course['enrollments'][0]['type']);
    // console.log(course['enrollments'][0]['type']);
    // console.log('course data found and dimensions set.');
    custom_ga('send', 'pageview');
    return
}

function googleAnalyticsCode(trackingID) {
    var userId, userRoles, attempts, courseId;
    custom_ga('create', trackingID, 'auto');
    userId = ENV["current_user_id"];
    userRoles = ENV['current_user_roles'];
    custom_ga('set', 'userId', userId);
    custom_ga('set', 'dimension1', userId);
    custom_ga('set', 'dimension2', userRoles);
    courseId = window.location.pathname.match(/\/courses\/(\d+)/);
    // console.log(courseId);
    if (courseId) {
        courseId = courseId[1];
        attempts = 0;
        try {
            let courses = getStorage('ga_enrollments')
            if (courses != null) {
                var course = parseCourses(courseId, courses);
                if (course === null) {
                    // console.log("course_id not found in cache, retrieving...")
                    coursesRequest(courseId).then(course => {
                        if (course === null) {
                            // console.log("course data not found")
                            custom_ga('set', 'dimension3', courseId);
                            custom_ga('send', 'pageview');
                        } else {
                            gaCourseDimensions(course)
                        }
                    });
                } else {
                    // console.log("course found in cache")
                    gaCourseDimensions(course)
                }
            } else {
                // console.log("cache not found, retrieving cache data")
                coursesRequest(courseId).then(course => {
                    if (course === null) {
                        // console.log("course data not found")
                        custom_ga('set', 'dimension3', courseId);
                        custom_ga('send', 'pageview');
                    } else {
                        gaCourseDimensions(course)
                    }
                });
            }
        } catch (err) {
            attempts += 1;
            if (attempts > 5) {
                // console.log('catching error and bailing out');
                custom_ga('set', 'dimension3', courseId);
                custom_ga('send', 'pageview');
                return;
            };
        };
    } else {
        // console.log('no course ID so... shrug.');
        custom_ga('send', 'pageview');
    };
};

googleAnalyticsCode("UA-34361385-4") // replace google analytics tracking id here

var trackOutboundLink = function (url, eventAction) {
    custom_ga('send', 'event', 'outbound', eventAction, url, {
        'transport': 'beacon',
        'hitCallback': function () { window.open(url); }
    });
}

$(document).ready(function () {

    // google analytics tracking code, provided by ITS Web Services

    // (function (i, s, o, g, r, a, m) {
    //     i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
    //         (i[r].q = i[r].q || []).push(arguments)
    //     }, i[r].l = 1 * new Date(); a = s.createElement(o),
    //         m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    // })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    // ga('create', 'UA-34361385-4', 'auto');
    // ga('send', 'pageview');


    // replace "email addresses" with "UNCG usernames or e-mail addresses" in [ + People ]

    var form = "form[data-view='createUsers']";
    $(document).on("click", "#addUsers", function () { // if the element exists
        $(form + " p").html("Type or paste a list of UNCG usernames below, separated by commas.<br><em>Note: only UNCG users may be added.</em>");
        $(form + " #user_list_textarea").attr("placeholder", "Enter UNCG usernames here, separated by commas").attr("aria-label", "Enter UNCG usernames here, separated by commas");
    });

    // add Starfish to Profile area
    var userId = ENV.current_user_id;

    var urlChunks = window.location.href.split("/");
    var domain = urlChunks[2];

    var icon = "icon-star";

    var linkId = "starfish";
    //var linkhref = "https://" + domain + "/users/" + userId + "/external_tools/334";
    var linkhref = "https://starfish.uncg.edu";
    var iconHtml = "<i class=" + icon + " style=\"color:white;\"></i> ";
    var linkText = "Starfish";

    // DRY method to add a Starfish link after the ePortfolios link, wherever it appears
    function addStarfishLinkAfterEportfolios(targetElementToClone, locationOfReplacementForTracking) {
        if (targetElementToClone.length > 0) {
            var newElement = targetElementToClone.clone();

            var oldHtml = newElement.html();
            // console.log(oldHtml);
            oldHtml = oldHtml.replace(/\/dashboard\/eportfolios/g, linkhref + "\" target=\"_blank\"" + " onclick=\"trackOutboundLink('" + linkhref + "', '" + locationOfReplacementForTracking + "'); return false;");
            // console.log(oldHtml);

            oldHtml = oldHtml.replace(/eportfolios/g, "starfish");
            oldHtml = oldHtml.replace(/ePortfolios/g, "Starfish");

            newElement.html(oldHtml);
            // console.log(newElement.children());
            newElement.appendTo(targetElementToClone.parent());
        }
    }

    // add Starfish link in flyout menu after Profile is clicked
    $(document).on('click', "#global_nav_profile_link", function () {
        // change in approach for v1.8
        setTimeout(function () {
            addStarfishLinkAfterEportfolios($("#nav-tray-portal a[href='/dashboard/eportfolios']").parent(), 'profileMenuFlyoutMenu');
        }, 400);
    });

    // add Starfish in all user submenu areas after page load
    addStarfishLinkAfterEportfolios($("#section-tabs a[href='/dashboard/eportfolios']").parent(), 'profileMenuStaticMenu');




    // alter NEW [ + People ] area... for 2/18 release
    var htmlTitle = $("title").html();
    if (htmlTitle.indexOf('Course Roster: ') != -1) {
        var radios = {};
        radios.email = "peoplesearch_radio_cc_path";
        radios.username = "peoplesearch_radio_unique_id";
        radios.sis_id = "peoplesearch_radio_sis_user_id";

        var buttonsToRemove = ["email", "sis_id"];

        var selectedButton = "username";

        function uncgEditLoginOptions() {

            $span = $("#peoplesearch_radio_unique_id").next().children()[1];
            if ($span !== undefined) {
                $span.innerHTML = "UNCG Username";
            }

            $span2 = $(".peoplesearch__instructions").children('span').children()[0];
            if ($span2 !== undefined) {
                $span2.innerHTML = "Add users by UNCG Username.<br><i>Note: only UNCG users may be added.</i>";
            }

            // $("label[for='" + radios[selectedButton] + "'] input").trigger('click');
            $("input#" + radios[selectedButton]).trigger('click');

            for (var x = 0; x < buttonsToRemove.length; x++) {
                var forValue = radios[buttonsToRemove[x]];

                $("label[for='" + forValue + "']").hide();
            }
        }

        $(document).on('click', "#addUsers", function () {
            setTimeout(uncgEditLoginOptions, 1);
        });

        $(document).on('click', "#addpeople_back", function () {
            setTimeout(uncgEditLoginOptions, 1);
        });
    }

});
