// add "what's new" button
$("#breadcrumbs").css({"flex":"3"}).after('<a href="http://courses.uncg.edu/?p=845" target="_blank" class="btn btn-primary" style="flex:1;">See what\'s new in the new Canvas UI!</a>');

var sidebarCheck = function() {
  if ($(".ic-sidebar-logo").length) {
    $(".ic-sidebar-logo").after('<a href="http://courses.uncg.edu/?p=845&amp;" target="_blank" class="btn btn-primary" style="margin-bottom: 25px;">See what\'s new in the new Canvas UI!</a>');
    clearInterval(sidebarInterval);
  }
};

var sidebarInterval = setInterval(sidebarCheck, 100);