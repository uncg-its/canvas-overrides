## UNCG CANVAS OVERRIDES

### WHAT IS THIS?

    * Canvas Override files used by UNCG
    * Per specifications provided by Instructure at http://guides.instructure.com/m/4214/l/259671?data-resolve-url=true&data-manual-id=4214
    * Version history for the JS and CSS changes are at the base of this README file

### WHO MAINTAINS THIS?

    * UNCG LMS Administrators, who are part of the Application Administration group within Information Technology Services

### QUESTIONS? COMMENTS? CONCERNS?

    * 6-TECH@uncg.edu or lms-admins-l@uncg.edu


### CSS VERSION HISTORY (most recent first)

    v1.7.4 changes
    -- CSS override removal for hiding Sync to SIS button on the Quizzes page. Again. [Matt]

    v1.7.3 changes
    -- Reintroducing CSS override to hide the Sync to SIS button on the Quizzes page. ::facepalm:: [Matt]

    v1.7.2 changes
    -- CSS override removal for hiding Sync to SIS button on the Quizzes page. [Matt]

    v1.7.1 changes
    -- CSS override (temporary, we hope) to hide the Sync to SIS button on the Quizzes page. [Matt]

    v1.7 changes
    -- removing manual CSS hiding of help option that contacted Instructure directly; this is now fully customizable in GUI, no longer needed. [Matt]

    v1.6 changes
    -- Kennethware has been removed entirely; UNCG did not adopt.
    -- mobile CSS file has been created - not currently in use.

    v1.5.1 changes
    -- change the kennethware CSS file to an absolute link instead of relative (new UI pulls CSS files onto Canvas servers instead of letting us choose a destination). [Matt]

    v1.5 changes
    -- adjustments for new UI
    -- removed the overrides for branding (since the Theme Editor exists in new UI)

    v1.4.1 changes:
    -- none

    v1.4 changes:
    -- adds support for KennethWare (USU Design tools)

    v1.3.3 changes:
    -- resizes quiz question points input boxes in SpeedGrader to accommodate Instructure's recent change to <input type="number">. Now displays 5 digits plus a decimal point. Before, 2 digits plus a decimal point was too much to fit.  [Matt]

    v1.3.2 changes:
    -- removes styling to #section-tabs-header-subtitle to fix padding issue (Instructure fixed it) [Matt]
    -- added version information (comment block at top)

    v1.3.1 changes:
    -- adds styling to #section-tabs-header-subtitle to fix padding issue (Instructure broke it) [Matt]

    v1.3 changes:
    -- adds extra styling to change the height of the grading scheme box to a max of 700px. This should allow for most grading schemes to be displayed without having to scroll inside a scrolling box. [Matt]
    -- adds extra styling to remove an overflow scroll from the course listing on a user account page [Matt]

    v1.2 changes:
    -- adds min-height to div#header.no-user so that non-authenticated users are shown a normal header height (for ePortfolios, e.g.) [Matt]

    v1.1 changes:
    -- hides links to Instructure support (Help menu and on Page Not Found) [Matt]
    -- css file formatting, annotations [Matt]


### JS VERSION HISTORY (most recent first)

    v1.9 changes
    -- update Google Analytics code to fix a breakage from 8/2019 on the Canvas side. [Matt]

    v1.8.1 changes
    -- and again, Starfish custom link problems. Canvas code changed the nesting level of the element. We have once again vanquished the offending code. [Matt]

    v1.8.0 changes
    -- Canvas code yet again broke Starfish custom link. We have again conquered it and are inserting a Starfish link in the flyout menu (tray) AND on all user-account-level pages in the left nav. [Matt and Nick]

    v1.7.3 changes
    -- Recent changes in Canvas code broke our Starfish menu item addition (the tray menu HTML structure changed, and rendering changed slightly). As a result, we needed to modify the traversal pattern for our addition of the extra <li> item, and add a timeout to the "add" operation.

    v1.7.2 changes
    -- Recent changes in Canvas code broke the [ + People ] override (auto-selecting UNCG username was not working, normal menu would appear when you dismiss and then re-click the modal). This version updates [ + People ] override to be compliant with recent changes in Canvas code.

    v1.7.1 changes
    -- updated Starfish link to point to new Starfish portal (retiring LTI tool) [Nick]

    v1.7 changes
    -- fixed Starfish addition to Account menu. Recent Canvas update broke this. [Matt]
    -- altered the new [ + People ] menu, which first appeared in the 2/18/2017 release. Renders the v1.3.3 change obsolete, but we can remove that with next version.

    v1.6 changes
    -- Kennethware has been removed entirely; UNCG did not adopt.
    -- mobile JS file has been created - not currently in use.

    v1.5 changes:
    -- adds Starfish link to the user profile pullout menu. NB - uses jQuery to mess around with a React element...might want to change this going forward, if we can.

    v1.4.1 changes:
    -- adds sniffer to left-hand bar so that Design Tools only show up if Template Wizard is active.

    v1.4 changes:
    -- adds support for KennethWare (USU Design tools)

    v1.3.3 changes:
    -- changes text in [ + People ] dialogue box to correlate accurately with UNCG procedures (e.g. adding users via UNCG username only)

    v1.3.2 changes:
    -- added version information (comment block at top)

    v1.3.1 changes:
    -- added Google Analytics tracking code (provided by ITS Web Services)
